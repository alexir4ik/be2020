<?php
require_once "../config.php";

    if (!empty($_POST['reg_id'])) {
        if (!empty($_POST['login']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['confirm_password'])) {
            $login = trim($_POST['login']);
            $email = $_POST['email'];
            $password = $_POST['password'];
            $passwordConfirm = $_POST['confirm_password'];

            $stmt1 = $pdo->prepare("SELECT `login` FROM `users_new`");
            $stmt1->execute();
            $arrLogin = $stmt1->fetchALL(PDO::FETCH_COLUMN);
            if (in_array($login, $arrLogin)) {
                echo "Такой логин уже существует";
            } else {
                $stmt1 = $pdo->prepare("SELECT `email` FROM `users_new`");
                $stmt1->execute();
                $arrEmail = $stmt1->fetchALL(PDO::FETCH_COLUMN);
                if (in_array($email, $arrEmail)) {
                    echo "Такая электронная почта уже используется";
                } else {
                    if ($password != $passwordConfirm) {
                        echo "Пароли не совпадают!";
                    } else {
                        $stmt = $pdo->prepare(
                            "INSERT INTO `users_new` ( `login`, `email`, `password`) VALUES ( :login, :email, :password)"
                        );
                        $stmt->execute(["login" => $login, "email" => $email, "password" => $password]);
                        $_SESSION['login'] = $login;
                        header("Location: mypage.php");
                        exit();
                        echo "Вы успешно зарегистрировались";
                    }
                }
            }
        }
    }


?>
<!doctype html>
<html lang=ru>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Форма регистрации</title>
</head>
<body>

<form action="registration.php" method="POST">
    <div>
        <label for="name">Введите логин</label><br>
        <input type="text" name="login" id="name" ><br><br>
    </div>
    <div>
        <label for="name">Введите электронную почту</label><br>
        <input type="email" name="email" id="name" ><br><br>
    </div>
    <div>
        <label for="password">Введите пароль</label><br>
        <input type="password" name="password" id="password" ><br><br>
    </div>
    <div>
        <label for="confirm_password">Подтвердите пароль</label><br>
        <input type="password" name="confirm_password" id="confirm_password" ><br><br>
    </div>

    <div>
        <input type="submit" name="reg_id" value="Зарегистрироваться">
    </div>
</form>

</body>
</html>
