<?php
require_once "../config.php";

    if (!empty($_POST)) {
        $stmt = $pdo->prepare("SELECT login, password FROM `users_new`");
        $stmt->execute();
        $arrLoginPass = $stmt->fetchAll();

        if (!empty($_POST['registration'])) {
            header("Location: registration.php");
            exit();
        }

        if (!empty($_POST['login']) && !empty($_POST['password'])) {
            $login = trim($_POST['login']);
            $pass = trim($_POST['password']);

            for ($i = 0; $i < count($arrLoginPass); $i++) {
                if ($login == $arrLoginPass[$i]['login'] && $pass == $arrLoginPass[$i]['password']) {
                    $_SESSION['login'] = $login;
                    header("Location: mypage.php");
                    exit;
                } /*else {
                    echo "Вас нет в базе данных";
                }*/

                if (!file_exists(ROOT_PATH . "/login/{$login}.txt")) {
                    $fpNewFile = fopen(ROOT_PATH . "/login/{$login}.txt", "w");
                    fputs($fpNewFile, "1");
                    fclose($fpNewFile);
                } else {
                    $fpNewFile = fopen(ROOT_PATH . "/login/{$login}.txt", "r+");
                    $count = fgets($fpNewFile);
                    $count++;
                    rewind($fpNewFile);
                    fputs($fpNewFile, $count);
                    fclose($fpNewFile);
                }
            }
        }
    }

?>
    <!doctype html>
    <html lang=ru>
    <head>
        <meta charset="utf-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Форма регистрации</title>
    </head>
    <body>

    <form action="login.php" method="POST">
        <div>
            <label for="name">Введите логин</label><br>
            <input type="text" name="login" id="name"><br><br>
        </div>

        <div>
            <label for="password">Введите пароль</label><br>
            <input type="password" name="password" id="password"><br><br>
        </div>

        <div>
            <input type="submit" name="button" value="Войти">
            <input type="submit" name="registration" value="Регистрация">
        </div>
    </form>

    </body>
    </html>
