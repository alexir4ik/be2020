<?php

    define("ROOT_PATH", dirname(__FILE__));
    define("DB_USER", "db_user");
    define ( "DB_PASS", "1111" );
    define ( "DB_NAME", "shopping" );
    $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ];
    $dsn = "mysql:host=localhost;port=3306;dbname=".DB_NAME.";charset=utf8";
    $pdo = new PDO ($dsn, DB_USER, DB_PASS, $opt);
    session_start();